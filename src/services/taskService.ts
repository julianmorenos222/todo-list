import { supabase } from '$lib/supabaseClient';

 const addTask = async (value: string) => {
    const { data, error } = await supabase.from('todos').insert([
        { title: value, completed: false }
    ])
    if (error) throw error
    return data
}

 const getAllTasks = async () => {
    const { data, error } = await supabase
        .from("todos")
        .select()
    if (error) throw error
    return { tasks: data };
}

const toggleOneTask = async (id: number | string, isChecked: boolean) => {
    const { data, error } = await supabase.from('todos').update({ completed: !isChecked }).eq('id', id)
    if (error) console.log("error", error);
    return { tasks: data };
}

const deleteTask = async (id: number | string) => {
    const { data, error } = await supabase.from('todos').delete().eq('id', id)
    if (error) console.log("error", error);
    return { tasks: data };
}

export { addTask, getAllTasks, toggleOneTask, deleteTask }