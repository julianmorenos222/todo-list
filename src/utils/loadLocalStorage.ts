import { theme } from "$lib/store/store";
export const loadLocalStorage = () => {
    const themeLocal = localStorage.getItem('theme')

    if (themeLocal) {
        theme.set(themeLocal)
    }
}