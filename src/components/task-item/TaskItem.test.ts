import { render, fireEvent, screen } from '@testing-library/svelte';
import { describe, it, expect, beforeEach, vi } from 'vitest';
import Task from './TaskItem.svelte'; 
import { store } from '$lib/store/store.ts'; 
import Checked from '../checked/Checked.svelte';

describe('Task', () => {

  beforeEach(() => {
    vi.spyOn(store, 'toggleOneTask'); 
    vi.spyOn(store, 'removeTask');
    });

 it('should render task text without strikethrough when not checked', () => {
    const text = 'Walk the dog';
    render(Task, { props: { text, isChecked: false,id:1 } });

    const taskText = screen.getByText(text);
    expect(taskText).toBeDefined();
    expect(taskText.classList).not.toContain('line-through'); 
  });
  it('should render task text and strikethrough when checked', () => {
    const text = 'Buy groceries';
    render(Task, { props: { text, isChecked: true,id:1 } });

    const taskText = screen.getByText(text);
    expect(taskText).toBeDefined();
    expect(taskText.classList).toContain('line-through'); 
  }); 


   it('should call store.toggleOneTask on Checked component click', async () => {
    const text = 'Finish homework';
    render(Task, { props: { text, id:1 } });
    const { getAllByRole} =  render(Checked, { props: {  isChecked: false } });
    
    const checkedComponent = getAllByRole('button')[0];
   if (!checkedComponent) {
      throw new Error('Checked component not found');
    }
    await fireEvent.click(checkedComponent); 

    expect(store.toggleOneTask).toHaveBeenCalledTimes(1);
    expect(store.toggleOneTask).toHaveBeenCalledWith(expect.any(Number)); 
  }); 

 it('should call store.removeTask on remove button click', async () => {
    const text = 'Clean the room';
    render(Task, { props: { text, id:1 } });

    const removeButton = screen.getAllByRole('button')[1];
    await fireEvent.click(removeButton);

    expect(store.removeTask).toHaveBeenCalledTimes(1);
    expect(store.removeTask).toHaveBeenCalledWith(expect.any(Number)); 
  });   
});
