import { render, screen, fireEvent } from '@testing-library/svelte';
import { describe, it, expect, vi, beforeEach } from 'vitest';
import Header from './Header.svelte'; 
import { theme } from '$lib/store/store.ts';

describe('Header', () => {
    render(Header);

    beforeEach(() => {
        vi.spyOn(theme, 'update');
     });
   it('should initially render the light theme icon', () => {

        const themeButton = screen.getByRole('button');
        const moonIcon = screen.getByRole('img');

        expect(themeButton).toBeDefined();
        expect(moonIcon.getAttribute('alt')).toBe('theme');
    }); 

     it('should toggle theme on button click and update local storage', async () => {

        const themeButton = screen.getByRole('button');
        await fireEvent.click(themeButton);

        expect(theme.update).toHaveBeenCalledTimes(1); 
        expect(theme.update).toHaveBeenCalledWith(expect.any(Function)); 
    }); 
});
