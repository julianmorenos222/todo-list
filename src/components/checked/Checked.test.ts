import { render} from '@testing-library/svelte';
import { describe, it, expect } from 'vitest';
import Checked from './Checked.svelte';

describe('Checked', () => {
  const { getByRole } = render(Checked);

  it('should render component', () => {
    const button = getByRole('button');
    expect(button).toBeTruthy();
  })

  it('should initially render unchecked', () => {
    const button = getByRole('button').children[0];
    expect(button).undefined;
  });

 });
