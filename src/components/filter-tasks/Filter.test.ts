import { render,screen, fireEvent } from '@testing-library/svelte';
import { describe, it, expect } from 'vitest';
import FilterTasks from './FilterTasks.svelte';

describe('Filter', () => {
    render(FilterTasks);
    
    it('should render filter options correctly', () => {
        const options = ['all', 'active', 'completed'];
        
    
        const buttons = screen.getAllByRole('button');
        expect(buttons.length).toBe(options.length); 
    
        buttons.forEach((button, index) => {
          expect(button.textContent?.toLowerCase()).toBe(options[index]);
        });
      });

      it('should activate the button when you click', async () => {
        const optionToClick = 'active';
        render(FilterTasks);
    
        const buttons = screen.getAllByRole('button');
        const targetButton = buttons.find((button) => button.textContent === optionToClick.charAt(0).toUpperCase() + optionToClick.slice(1));
        
        if (!targetButton) {
          throw new Error(`Button with text "${optionToClick}" not found`);
        }
        expect(targetButton.classList).not.toContain('text-blue-500');
        console.log(targetButton)
        await fireEvent.click(targetButton);
        expect(targetButton.classList).toContain('text-blue-500'); 
      });
})