import { render, screen } from '@testing-library/svelte';
import { describe, it, expect } from 'vitest';
import Footer from './Footer.svelte'; 

describe('Footer', () => {
    render(Footer);
  it('should render copyright text and link to GitLab', () => {

    const copyright = screen.getByText(/© 2024/i);
    expect(copyright).toBeDefined();

    const gitlabLink = screen.getByRole('link', { name: /Julian Moreno/i });
    expect(gitlabLink).toBeDefined();
    expect(gitlabLink.getAttribute('href')).toContain('https://gitlab.com/julianmorenos222/todo-list');
    expect(gitlabLink.getAttribute('target')).toContain('_blank');
  });

  it('should render GitLab logo image', () => {

    const gitlabLogo = screen.getByRole('img');
    console.log(gitlabLogo)
    expect(gitlabLogo).toBeDefined();
    expect(gitlabLogo.tagName).toBe('IMG');
  });
});
