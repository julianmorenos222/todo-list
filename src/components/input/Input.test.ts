import { render, fireEvent, screen } from '@testing-library/svelte';
import { describe, it, expect, beforeEach, vi } from 'vitest';
import TodoInput from './Input.svelte';
import { store } from '$lib/store/store.ts';



describe('TodoInput', () => {
    render(TodoInput);

    beforeEach(() => {
        vi.spyOn(store, 'addTask');
        vi.spyOn(store, 'toggleAllCompleted');
    });

    it('Should add a new task when Enter is pressed', async () => {
    
        const input = screen.getByPlaceholderText('Create a new todo...');
        const value = 'Finish project';

        await fireEvent.input(input, { target: { value } });
        await fireEvent.keyDown(input, { key: 'Enter', keyCode: 13 });

        expect(store.addTask).toHaveBeenCalledTimes(1);      
      });
    
    it('should not add a new task on Enter key press with empty value', async () => {
        
        const input = screen.getByPlaceholderText('Create a new todo...');
        
        await fireEvent.keyDown(input, { key: 'Enter',keyCode: 13 });

        expect(store.addTask).not.toHaveBeenCalled();
    });

    it('should clear input value and set isChecked to false after adding a task', async () => {
        

        const input: HTMLInputElement = screen.getByPlaceholderText('Create a new todo...');
        const value = 'Finish project';

         await fireEvent.input(input, { target: { value } });  
         await fireEvent.keyDown(input, { key: 'Enter', target: { value },keyCode: 13 });

        expect(input.value).toBe('');
    });
});