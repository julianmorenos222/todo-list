import { createClient } from '@supabase/supabase-js'
import {PUBLIC_URL_PROJECT,PUBLIC_KEY_ANON} from '$env/static/public'

export const supabase = createClient(PUBLIC_URL_PROJECT, PUBLIC_KEY_ANON)

