import { writable } from 'svelte/store';
import { loadLocalStorage } from '../../utils/loadLocalStorage.ts'
import  {type TaskInterface}  from '../../types/types.ts';

function createTasks() {
    const { subscribe, set, update } = writable<TaskInterface[]>([]);
    const addTask = (item:TaskInterface) => {
        //actualizo el localStorage :)
        update((tasks) => {
            localStorage.setItem('tasks', JSON.stringify([...tasks, item]))
            return [...tasks, item]
        })

    }
    const setTasks = (tasks: TaskInterface[]) => set(tasks)
    const deleteCompletedTasks = () => {
        update((tasks) => {
            const filterTasks = tasks.filter((t) => !t.completed)
            //actualizo el localStorage :)
            localStorage.setItem('tasks', JSON.stringify(filterTasks))
            return filterTasks
        })
    }

    const toggleOneTask = (id: number | string) => {
        let newTasks;
        //cambio el valor de completed
        update((tasks) => {
            newTasks = tasks.map((t) => t.id === id ? { ...t, completed: !t.completed } : t)
            return newTasks

        })
        //actualizo el localStorage :)
        localStorage.setItem('tasks', JSON.stringify(newTasks))

    }
    const toggleAllCompleted = () => {
        update((tasks) => {
            const allCompleted = tasks.every((t) => t.completed)
            const newTasks = tasks.map((t) => ({ ...t, completed: !allCompleted }))
            //actualizo el localStorage :)
            localStorage.setItem('tasks', JSON.stringify(newTasks))
            return newTasks
        })
    }

    const removeTask = (id: number | string) => update((tasks) => {
        //filtro por id
        const filterTasks = tasks.filter((t) => t.id !== id)
        //actualizo el localStorage :)
        localStorage.setItem('tasks', JSON.stringify(filterTasks))
        return filterTasks
    })

    const filterTasks = (type: string) => {
        loadLocalStorage()
        update((tasks) => {
            switch (type) {
                case 'completed':
                    return tasks.filter((task) => task.completed);
                case 'active':
                    return tasks.filter((task) => !task.completed);
                default:
                    return tasks;
            }
        })
    };

    return {
        subscribe,
        set,
        update,
        addTask,
        removeTask,
        toggleOneTask,
        setTasks,
        deleteCompletedTasks,
        filterTasks,
        toggleAllCompleted
    }
}
export const store = createTasks()
export const theme = writable<string>("dark")
