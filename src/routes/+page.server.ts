import { getAllTasks } from "../services/taskService";

export async function load() {
  const { tasks } = await getAllTasks()
  return { tasks }
}